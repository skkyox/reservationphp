<?php require 'Resto.php'; ?>
<?php require 'Reservation.php'; ?>
<?php require 'Client.php'; ?>

<?php
/**
 * Created by PhpStorm.
 * User: lesom
 * Date: 06/06/2018
 * Time: 15:31
 */

$nom = isset($_POST['nom']) ? $_POST['nom'] : '';
$restaurant = isset($_POST['restaurant']) ? $_POST['restaurant'] : '';
$date = isset($_POST['date']) ? $_POST['date'] : '';
$hour = isset($_POST['hour']) ? $_POST['hour'].':00' : '';
$nbPersonnes = isset($_POST['nbPersonnes']) ? $_POST['nbPersonnes'] : '';
$client = new Client($nom);
$client->RecClient();
$idClient = $client->GetIdClient();
$reservation = new Reservation(intval($idClient[0]['Id_Client']),intval($restaurant), intval($nbPersonnes), $date, $hour);
var_dump(intval('20:00:00'));
if($reservation->CheckPlace()){
    $reservation->RecReservation();
}

?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" href="public/UIKit/css/uikit.min.css"/>
        <link rel="stylesheet" href="public/UIKit/css/uikit-rtl.min.css"/>
        <link href="https://fonts.googleapis.com/css?family=Courgette" rel="stylesheet">
        <link rel="stylesheet" href="public/css/index.css"/>
        <title>Reserve your restaurant</title>
    </head>
    <body>
    <header>
        <h1>Reserve a dinner</h1>
    </header>
    <div class="containerBanner">
        <div id="reservationContainer" class="uk-container">
            <form action="" method="post" class="uk-grid-small uk-grid formResa">
                <div class="uk-width-1-5@s">
                    <label for="nom">Entrez votre nom</label>
                    <input required name="nom" id="nom" class="uk-input" type="text" placeholder="Nom">
                </div>
                <div class="uk-width-1-5@s">
                    <div class="uk-margin">
                        <label class="uk-form-label" for="form-horizontal-select">Select</label>
                        <div class="uk-form-controls">
                            <select required name="restaurant" class="uk-select" id="form-horizontal-select">
                                <?php
                                $resto = new Resto();
                                $allResto = $resto->ListResto();
                                foreach ($allResto as $restaurant) { ?>
                                    <option value="<?php echo $restaurant['Id_Resto'] ?>"><?php echo $restaurant['Nom_Resto'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-6@s">
                    <label for="">Nombre de personnes</label>
                    <input required name="nbPersonnes" id="nbPersonnes" class="uk-input" max="30" type="number">
                </div>
                <div class="uk-width-1-5@s">
                    <label for="">Choisissez une date</label>
                    <input  required name="date" id="date" class="uk-input" type="date">
                </div>
                <div class="uk-width-1-5@s">
                    <label for="">Choisissez une heure</label>
                    <input  required name="hour" id="hour" class="uk-input" type="time">
                </div>
                <div class="uk-width-1-6@s uk-align-center">
                    <button class="uk-button uk-button-primary" type="submit">Réserver</button>
                </div>
            </form>
        </div>
    </div>
    <script type="text/javascript" src="public/UIKit/js/uikit.min.js/"></script>
    <script type="text/javascript" src="public/UIKit/js/uikit-icons.min.js"></script>
    <script type="text/javascript" src="public/js/index.js"></script>
    </body>
    </html>
