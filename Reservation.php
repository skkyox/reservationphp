<?php
/**
 * Created by PhpStorm.
 * User: kravo
 * Date: 06/06/2018
 * Time: 16:05
 */

class Reservation
{

    private $Id_NomClient;
    private $Id_NomResto;
    private $NbrPers;
    private $Date;
    private $Heure;

    public function __construct($IdClient, $IdResto, $NbrPerso, $Date, $Heure)
    {
        $this->Id_NomClient = $IdClient;
        $this->Id_NomResto = $IdResto;
        $this->NbrPers = $NbrPerso;
        $this->Date = $Date;
        $this->Heure = $Heure;

    }


    public function RecReservation()
    {

        $dbh = new PDO('mysql:host=127.0.0.1;dbname=reservationphp', 'root', '');

        $req = "INSERT INTO `Reservation` (`Id_Reservation`, `Date_Reservation`, `Heure_Reservation`, `Id_Client_Reservation`, `Id_Resto_Reservation`,`NbrPers_Reservation`) VALUES (NULL, '".$this->Date."' , '".$this->Heure."' , :Client , :Resto, :NbrPerso);";

        $query = $dbh->prepare($req);
        $query->bindParam(':Client',$this->Id_NomClient);
        $query->bindParam(':Resto',$this->Id_NomResto);
        $query->bindParam(':NbrPerso',$this->NbrPers);
        $query->execute();

    }

    public function CheckPlace()
    {
        $dbh = new PDO('mysql:host=127.0.0.1;dbname=reservationphp', 'root', '');
        $req = "SELECT SUM(NbrPers_Reservation)AS 'Reservationprevu'  FROM reservation WHERE Heure_Reservation = '".$this->Heure."' And Date_Reservation='".$this->Date."' AND Id_Resto_Reservation =".$this->Id_NomResto;
        $row = $dbh->query($req);
        $Prevu = $this->NbrPers;
        $results = $row->fetchAll();
        foreach ($results as $result) {
            $Prevu += $result['Reservationprevu'];
        }

        $req = "SELECT PersonneMax FROM resto WHERE id_Resto = ".$this->Id_NomResto;
        $row = $dbh->query($req);
        $results = $row->fetchAll();
        $DispoMax = 30;
        foreach ($results as $result) {
            $DispoMax = $result['PersonneMax'];
        }

        if($DispoMax - $Prevu >0 ){
            return true;
        }

        return false;
    }


}